# My VSCode Config

🤓 Convenient VSCode settings for improved DevEx 💻

## Setup
1. Your VSCode settings are conveniently stored in a JSON file called settings. json. 
2. To edit your settings in settings.json , start by opening the Command Palette with CMD/CTRL + SHIFT + P.
3. Inside the Command Pallette box, type in "Settings (JSON)" and select the first option.
4. Copy the contents of [settings.json](https://bitbucket.org/modalitsu/vscode-settings/src/master/settings.json), and paste them in your own configuration file.

## settings.json

```json

{    
    "workbench.startupEditor": "newUntitledFile",
    "editor.acceptSuggestionOnEnter": "smart",
    "editor.cursorBlinking": "smooth",
    "editor.cursorSmoothCaretAnimation": true,
    "editor.formatOnPaste": true, //Automatically format text when pasting & saving.
    "editor.formatOnSave": true, //Automatically format text when pasting & saving.
    "editor.mouseWheelZoom": true, //Enable ctrl + mousewheel for zooming.
    "explorer.confirmDelete": false, //Disable confirm prompt when deleting files.
    "explorer.confirmDragAndDrop": false, //Disable confirm prompt when moving files.
    "explorer.sortOrder": "type", //Sort files by type.
    "files.associations": {
        "*.svelte": "html" //Treat .svelte files as HTML.
    },
    "files.insertFinalNewline": true, //Insert empty line at the end of files.
    "files.trimFinalNewlines": true, //Trim excess new lines.
    "javascript.updateImportsOnFileMove.enabled": "always",
    "terminal.integrated.fontSize": 30,
    "terminal.integrated.rendererType": "dom",
    "terminal.integrated.shell.windows": "C:\\WINDOWS\\System32\\cmd.exe",
    "typescript.updateImportsOnFileMove.enabled": "always",
    "window.title": "${activeEditorMedium}${separator}${rootName}", //Window name = actual file path.
    "window.zoomLevel": -1,
    "workbench.editor.highlightModifiedTabs": true, //Unsaved filetab lights up.
    "workbench.editor.scrollToSwitchTabs": true,
    "editor.suggestSelection": "first",
    "vsintellicode.modify.editor.suggestSelection": "automaticallyOverrodeDefaultValue" //Switch editor tabs via mouse wheel.
}

```


# Extensions
In addition to `settings.json` I also use the following extensions. To install them do the following:

1. Bring up the Extensions view by clicking on the Extensions icon in the Activity Bar on the side of VS Code (Ctrl+Shift+X)
2. Search for whatever extension you need from the list below:

## Must-Have Extensions
1. ESLint
2. Python extension for Visual Studio Code
3. Bracket Pair Colorizer
4. Visual Studio Code Remote - Containers
5. Visual Sutdio IntelliCode


### Logs
Changelogs can be found under revisions [here](https://bitbucket.org/arienshibani/vscode-settings/commits/)
